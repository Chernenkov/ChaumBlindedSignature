import org.apache.commons.codec.digest.DigestUtils;

import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class BlindRsa {
    // alice key pair
    static KeyPair aliceKeyPair;

    // alice private key d
    static BigInteger alicePrivate;

    // alice public key e
    static BigInteger alicePublic;

    // Key pair's modulus
    static BigInteger N;

    // first message Bob sends to Alice, masked = H(msg) * r^e mod N
    private static BigInteger masked;

    // Alice's message to Bob, signed = masked^d mod N
    private static BigInteger signed;


    public static void main(String[] args) {
        try {
            // get current time in milliseconds
            long start = System.currentTimeMillis();


            // call Alice's function to produce a key pair (N, e ,d), and save it in alicePair variable
            aliceKeyPair = new KeyPair(1024); // #

            // get the private key d out of the key pair Alice produced
            alicePrivate = aliceKeyPair.getSecretExponent(); // #

            // get the public key e out of the key pair Alice produced
            alicePublic = aliceKeyPair.getOpenExponent(); // #

            // get the modulus of the key pair produced by Alice
            N = aliceKeyPair.getModule(); // #

            Bob.setFile("/home/alex/IdeaProjects/BlindRSA/files/qrTransaction.gif");
//            Bob.setFile("/home/alex/IdeaProjects/BlindRSA/files/textTransaction");

            // call Bob's function calculateMu with alice Public key as input to calculate masked,
            // and store it in masked variable, result is H(msg)*r^e
            masked = Bob.calculateMu();

            // call Alice's function calculateMuPrime with masked produced earlier by Bob as input,
            // to calculate  masked' and store it to signed  variable, result is (H(msg) * r^e)^d =
            // H(msg)^d*r
            signed = Alice.calculateMuPrimeWithChineseRemainderTheorem(masked);
//            System.out.println("(H(m)^d)*r: " + signed);

            // call Bob's function signatureCalculation with signed as input and calculate the signature,
            // then store it in sig variable, result is
            String sig = Bob.signatureCalculation(signed);

            Path path = Paths.get("/home/alex/IdeaProjects/BlindRSA/files/qrTransaction.gif");
            byte[] data = Files.readAllBytes(path);
            String message = DigestUtils.shaHex(data);
            byte[] msg = message.getBytes("UTF8");
            BigInteger md = new BigInteger(msg);

            md = Alice.calculateMuPrimeWithChineseRemainderTheorem(md);
            System.out.println("H(m)^d counted: " + md);

            // Bob is checking if the signature he got from Alice is valid,
            // that can be easily computed because (m^d)^e modN = m
            Bob.verify(sig);

            System.out.println();
            long elapsedTimeMillis = System.currentTimeMillis() - start;
            System.out.println("Program executed in " + elapsedTimeMillis + " milliseconds");
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
// /home/alex/IdeaProjects/BlindRSA/files/qrTransaction.gif
// /home/alex/IdeaProjects/BlindRSA/files/textTransaction