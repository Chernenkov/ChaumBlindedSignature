import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.RSAKeyGenParameterSpec;

// keypairs generation
// inverse
/**
 * The class AliceRSA represents Alice who creates an RSA keypair and can issue digital signatures
 */

public class Alice {

    /**
     * Produces and returns an RSA keypair (N,e,d)
     * N: Modulus, e: Public exponent, d: Private exponent
     * The public exponent value is set to 3 and the keylength to 2048
     * @return RSA keypair
     */
    public static KeyPair produceKeyPair(int keySize, int pubExp) {
        try {
            // get rsa key generator
            KeyPairGenerator rsaKeyPairGenerator = KeyPairGenerator.getInstance("RSA");

            //set the parameters for they key, key length=2048, public exponent=65537
            RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(keySize, BigInteger.valueOf(pubExp));

            //initialise generator with the above parameters
            rsaKeyPairGenerator.initialize(spec);

            //generate the key pair, N:modulus, d:private exponent
            KeyPair keyPair = rsaKeyPairGenerator.generateKeyPair();

            return (keyPair);  //return the key pair produced (N,e,d)

        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Calculate masked' using the Chinese Remainder Theorem for optimization
     * isomorphism property: f(x+y)=f(x)+f(y) -> split the masked^d (mod N) in two:
     * one mode p, one mode q, then combine the results to calculate muPrime
     * @param mu - received from Bob
     * @return masked' - an encrypted with Alice's (N,d) masked
     */
    public static BigInteger calculateMuPrimeWithChineseRemainderTheorem(BigInteger mu) {
        try {
            //get modulus N
            BigInteger N = BlindRsa.N;

            //get the prime number p used to produce the key pair
//            BigInteger P = BlindRsa.alicePrivate.getPrimeP(); // !!!!!
            BigInteger P = BlindRsa.aliceKeyPair.getP();

            //get the prime number q used to produce the key pair
            BigInteger Q = BlindRsa.aliceKeyPair.getQ();
            //We split the masked^d mod N in two , one mode p , one mode q


            //calculate p inverse modulo q
            BigInteger PinverseModQ = BlindRsa.aliceKeyPair.inverseMod(P, Q);

            //calculate q inverse modulo p
            BigInteger QinverseModP = BlindRsa.aliceKeyPair.inverseMod(Q, P);

            //get private exponent d
            BigInteger d = BlindRsa.alicePrivate;
            //We split the message masked in to messages m1, m2 one mod p, one mod q

            //calculate m1=(masked^d modN)modP
            BigInteger m1 = BlindRsa.aliceKeyPair.powMod(mu, d, N).mod(P);

            //calculate m2=(masked^d modN)modQ
            BigInteger m2 = BlindRsa.aliceKeyPair.powMod(mu, d, N).mod(Q);

            //We combine the calculated m1 and m2 in order to calculate signed
            //We calculate signed: (m1*Q*(Q^-1(mod P)) + m2*P*(P^-1(modQ))) mod N where N = P*Q
            BigInteger muprime = ((m1.multiply(Q).multiply(QinverseModP))
                    .add(m2.multiply(P).multiply(PinverseModQ)))
                    .mod(N);

            return muprime;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

