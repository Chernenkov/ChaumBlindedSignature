import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

public class KeyPair {
    private boolean pOk;
    private boolean qOk;
    private BigInteger p;
    private BigInteger q;
    private BigInteger e;
    private BigInteger d;
    private BigInteger n;
    private BigInteger eiler;
    private int keysize;

    public KeyPair(int keysize) {
        this.keysize = keysize;
        p = BigInteger.ZERO;
        q = BigInteger.ZERO;
        System.gc();
        calculateP();
        System.gc();
        calculateQ();
        calculateN();
        calculateEuler(p, q);
        calculateE();
        calculateD(e, eiler);
    }

    private void calculateP() {
        Random rand = new Random(System.currentTimeMillis());
        p = BigInteger.probablePrime(keysize, rand);
        pOk = isPrime(p, 10000);
        while(!pOk) {
            p = BigInteger.probablePrime(keysize, rand);
            pOk = isPrime(p, 10000);
        }
        System.out.println("P is prime: " + pOk);
    }

    private void calculateQ() {
        Random rand = new Random(System.currentTimeMillis());
        q = BigInteger.probablePrime(keysize, rand);
        qOk= isPrime(q,10000);
        while (!qOk) {
            q = BigInteger.probablePrime(keysize, rand);
            qOk= isPrime(q,10000);
        }
        System.out.println("Q is prime: " + qOk);
    }

    private void calculateN() {
        if((q.compareTo(BigInteger.ZERO) == 0) || (p.compareTo(BigInteger.ZERO) == 0) ) {
            throw new IllegalArgumentException("P or Q not generated yet");
        }
        n = q.multiply(p);
    }


    private void calculateEuler(BigInteger p, BigInteger q) {
        eiler = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
    }

    private void calculateE() {
        BigInteger gcd;
        for (e = BigInteger.valueOf(2); (e.compareTo(eiler) < 0); e = e.add(BigInteger.valueOf(System.currentTimeMillis()))) {
            gcd = e.gcd(eiler);
            if(gcd.compareTo(BigInteger.ONE) == 0) {
                return;
            }
        }
    }

    private void calculateD(BigInteger e, BigInteger eiler) {
        d = inverseMod(e, eiler);
    }



    private BigInteger[] extendedEuclid(BigInteger a, BigInteger b) {
        BigInteger[] answer = new BigInteger[3];
        BigInteger ax, by;

        if(b.equals(BigInteger.ZERO)) {
            answer[0] = a;
            answer[1] = BigInteger.ONE;
            answer[2] = BigInteger.ZERO;
            return answer;
        }

        answer = extendedEuclid(b, a.mod(b));
        ax = answer[1];
        by = answer[2];
        answer[1] = by;
        BigInteger tmp = a.divide(b);
        tmp = by.multiply(tmp);
        answer[2] = ax.subtract(tmp);
        return answer;
    }

    public BigInteger inverseMod(BigInteger a, BigInteger b) {
        BigInteger [] result = extendedEuclid(a, b);

        if(result[1].compareTo(BigInteger.ZERO) > 0) {
            return result[1];
        } else {
            return result[1].add(b);
        }
    }

    public boolean isPrime(BigInteger n, int iteration) {
        // base case
        if (n.equals(BigInteger.ZERO) || n.equals(BigInteger.ONE))
            return false;
        // 2 is prime
        if (n.equals(new BigInteger("2")))
            return true;
        // an even number other than 2 is composite
        if (n.mod(new BigInteger("2")).equals(BigInteger.ZERO))
            return false;

        BigInteger s = n.subtract(new BigInteger("1"));
        while (s.mod(new BigInteger("2")).equals(BigInteger.ZERO))
            s = s.divide(new BigInteger("2"));

        Random rand = new Random();
        for (int i = 0; i < iteration; i++)
        {
            BigInteger r = new BigInteger(128, new SecureRandom());
            BigInteger a = r.mod(n.subtract(new BigInteger("1"))).add(new BigInteger("1"));
            BigInteger temp = s;
            BigInteger mod = a.modPow(temp, n);
            while (!(temp.equals(n.subtract(BigInteger.ONE))) &&
                    !(mod.equals(BigInteger.ONE)) &&
                    !(mod.equals(n.subtract(BigInteger.ONE)))) {

                mod = mod.modPow(mod, n); //mulMod(mod, mod, n);
                temp = temp.multiply(new BigInteger("2")); //temp * 2;
            }
            if (!(mod.equals(n.subtract(BigInteger.ONE)))
                    && (temp.mod(new BigInteger("2")).equals(BigInteger.ZERO)))
                return false;
        }
        return true;
    }

    public BigInteger powMod(BigInteger x, BigInteger n, BigInteger mod) {
        BigInteger res = BigInteger.ONE;
        for (BigInteger p = x; n.compareTo(BigInteger.ZERO) > 0; n = n.shiftRight(1), p = (p.multiply(p)).mod(mod)) {
            if (!(n.and(BigInteger.ONE)).equals(BigInteger.ZERO)) {
                res = (res.multiply(p).mod(mod));
            }
        }
        return res;
    }

    public BigInteger getOpenExponent() {
        return e;
    }

    public BigInteger getSecretExponent() {
        return d;
    }

    public BigInteger getModule() {
        return n;
    }

    public String getPublicKey() {
        return "{" + e + ", " + n + "}";
    }

    public String getPrivateKey() {
        return "{" + d + ", " + n + "}";
    }

    public BigInteger getQ() {
        return q;
    }

    public BigInteger getP() {
        return p;
    }

    public BigInteger getN() {
        return n;
    }

    public BigInteger getEiler() {
        return eiler;
    }
}
