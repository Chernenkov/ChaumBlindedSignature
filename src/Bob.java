import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Objects;

/**
 * The class Bob represents Bob who wishes to get a signature from Alice over his message
 * but without Alice seeing the actual message
 */
public class Bob {
    static BigInteger r;

    static BigInteger m;
    private static String filePath = "";

//    private String filePath = "";
    /**
     * Calculates and returns the masked
     * Bob uses Alice's public key and a random value r, such that r is relatively prime to N
     * to compute the blinding factor r^e mod N. Bob then computes the blinded message masked = H(msg) * r^e mod N
     * It is important that r is a random number so that masked does not leak any information about the actual message
     *
     * @return the blinded message masked
     *
     */

    public static BigInteger calculateMu() { // RSAPublicKey alicePublic
        try {
            String message;
            if(!Objects.equals(filePath, "")) {
                Path path = Paths.get(filePath);
                byte[] data = Files.readAllBytes(path);
                //calculate SHA hash over message
                message = DigestUtils.shaHex(data);
            } else {
                //calculate SHA hash over message
                message = DigestUtils.shaHex("Hello");
            }

            //get the bytes of the hashed message
            byte[] msg = message.getBytes("UTF8");

            //create a BigInteger from the bytes of the message
            m = new BigInteger(msg);
            System.out.println("H(msg):" + m);

            //get the public exponent 'e' of Alice's key pair
//            BigInteger e = BlindRsa.alicePublic.getPublicExponent(); // !!!!!
            BigInteger e = BlindRsa.alicePublic;
            // get modulus 'N' of the key pair
            BigInteger N = BlindRsa.N;

            // Generate a random number 'r',
            // so that it belongs to Zn, > 1 and is invertible in Zn
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");

            // r stored in bytes
            byte[] randomBytes = new byte[10];

            // BigInteger.ONE object equal to 1,
            // to compare it with the r to verify r > 1
            BigInteger one = BigInteger.ONE;

            BigInteger gcd = BigInteger.ZERO;

            // make r = 1 (mod N)
            do {
                //generate random bytes
                random.nextBytes(randomBytes);

                // BigInteger from the generated random bytes presenting 'r' number
                r = new BigInteger(randomBytes);

                // calculate the gcd for random number 'r' and the modulus
                gcd = r.gcd(BlindRsa.aliceKeyPair.getModule());
            } while (!gcd.equals(one) ||
                    r.compareTo(BlindRsa.N) >= 0 ||
                    r.compareTo(one) <= 0);
            //repeat until getting an 'r' that belongs to Zn and > 1 and == 1 mod N

            //Bob now computes masked = H(msg) * r^e mod N - a masked hash
            BigInteger mu = ((r.modPow(e, N)).multiply(m)).mod(N);

            return mu;

        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Set file path for which you want to count blind signature.
     * If it's not set, the system works on default string "Hello".
     * @param path
     */
    public static void setFile(String path) { filePath = path; }

    /**
     * Calculate signature over masked'
     * Bob receives the signature over the blinded message that he sent to Alice
     * and removes the blinding factor to compute the signature over his actual message
     * @param muPrime
     * @return signature
     */
    public static String signatureCalculation(BigInteger muPrime) {

        try {
            //get modulus of the key pair
            BigInteger N = BlindRsa.N;

            // Bob computes sig = masked'*r^-1 mod N,
            // inverse of r mod N multiplied with muPrime mod N, to remove the blinding factor
            BigInteger s = r.modInverse(N).multiply(muPrime).mod(N);
//            System.out.println("H(msg)^d(demasked): " + s);
            //encode with Base64 encoding to be able to read all the symbols
            byte[] bytes = new Base64().encode(s.toByteArray());

            //make a string based on the byte array representing the signature
            String signature = (new String(bytes));

            System.out.println("Signature produced with Blind RSA procedure for message: "
                    + new String(m.toByteArray()) + " is: ");

            System.out.println(signature);

            return signature;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Checks if the signature received from Alice, is a valid signature for the message given,
     * this can be easily computed because (m^d)^e (mod N) = m
     * @param signature
     */
    public static void verify(String signature) {
        try {
            //create a byte array extracting the bytes from the signature
            byte[] bytes = signature.getBytes();

            // decode the bytes with Base64 decoding (remember we encoded with base64 earlier)
            byte[] decodedBytes = new Base64().decode(bytes);

            // create the BigInteger object based on the bytes of the signature
            BigInteger sig = new BigInteger(decodedBytes);

            //get the public exponent of Alice's key pair
//            BigInteger e = BlindRsa.alicePublic.getPublicExponent(); // !!!!!
            BigInteger e = BlindRsa.alicePublic;

            //get the modulus of Alice's key pair
            BigInteger N = BlindRsa.N;

            // calculate sig^e (mod N), if we get back the initial message
            // that means that the signature is valid, because (m^d)^e (mod N) = m
            System.out.println("H(m)^d: " + sig);
//            BigInteger signedMessageBigInt = sig.modPow(e, N); // !!!!!
            BigInteger signedMessageBigInt = BlindRsa.aliceKeyPair.powMod(sig, e, N);

            System.out.println("H(msg): " + signedMessageBigInt);

            //create a String based on the result of the above calculation
            String signedMessage = new String(signedMessageBigInt.toByteArray());
            System.out.println();

            //create a String based on the initial message we wished to get a signature on
            String initialMessage = new String(m.toByteArray());

            //compare the two Strings, if they are equal the signature we got is a valid
            if (signedMessage.equals(initialMessage))
            {
                //print message for successful verification of the signature
                System.out.println("Verification of signature completed successfully");
            }
            else {
                // print message for unsuccessful verification of the signature
                System.out.println("Verification of signature failed");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
